module.exports = {
    arrowParens: "avoid",
    endOfLine: "auto",
    tabWidth: 4,
    trailingComma: "all",
    quoteProps: "consistent",
};
