module.exports = {
    testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|js?|tsx?|ts?)$",
    transform: {
        "^.+\\.js?$": "babel-jest",
        "^.+\\.(vue)$": "vue-jest",
    },
    testPathIgnorePatterns: ["<rootDir>/dist/", "<rootDir>/node_modules/"],
    moduleFileExtensions: ["js", "vue"],
    collectCoverage: true,
};
