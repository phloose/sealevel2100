import Vue from "vue";
import { shallowMount } from "@vue/test-utils";

import ScenarioInfoBox from "../../../src/sealevel2100/components/Scenario/ScenarioInfoBox.vue";
import EventBus from "../../../src/sealevel2100/EventBus";

describe("ScenarioInfoBox", () => {
    it("is not visible without scenarios_loaded and items", async () => {
        const comp = shallowMount(ScenarioInfoBox, {
            propsData: {
                id: "test-info-box",
                title: "Test Title",
                kind: "info",
            },
        });
        await Vue.nextTick();
        expect(comp.html()).toEqual("");
    });
    it("is visible if scenarios_loaded and items are truthy", async () => {
        const comp = shallowMount(ScenarioInfoBox, {
            propsData: {
                id: "test-info-box",
                title: "Test Title",
                kind: "info",
                items: [{ title: "A title", content: "some content" }],
            },
        });
        EventBus.$emit("scenarios-loaded");
        await Vue.nextTick();
        expect(comp.html()).toMatchInlineSnapshot(`
            "<info-box-stub id=\\"test-info-box\\">
              <h4 class=\\"info-box-heading\\">Test Title</h4>
              <ul id=\\"test-info-box-list\\">
                <li>
                  A title - some content
                </li>
              </ul>
            </info-box-stub>"
        `);
    });
    it("emits event 'update' when global event 'slider-changed' is emitted", () => {
        const comp = shallowMount(ScenarioInfoBox, {
            propsData: {
                id: "test-info-box",
                title: "Test Title",
                kind: "info",
            },
        });
        EventBus.$emit("slider-changed", {
            scenario: "scenario1",
            dikeScenario: "dikeScenario1",
        });
        expect(comp.emitted("update")).toBeTruthy();
        expect(comp.emitted("update")[0]).toEqual([
            {
                scenario: "scenario1",
                dikeScenario: "dikeScenario1",
            },
        ]);
    });
    it("renders slots with access to items", async () => {
        const comp = shallowMount(ScenarioInfoBox, {
            propsData: {
                id: "test-info-box",
                title: "Test Title",
                kind: "info",
                items: [{ title: "A title", content: "some content" }],
            },
            scopedSlots: {
                default: `
                    <ol>
                        <li v-for="item in props.content" :key="item.title" >
                            {{ item.title }} with {{ item.content }}
                        </li>
                    </ol>
                `,
            },
        });
        EventBus.$emit("scenarios-loaded");
        await Vue.nextTick();
        expect(comp.html()).toMatchInlineSnapshot(`
            "<info-box-stub id=\\"test-info-box\\">
              <h4 class=\\"info-box-heading\\">Test Title</h4>
              <ol>
                <li>
                  A title with some content
                </li>
              </ol>
            </info-box-stub>"
        `);
    });
});
