import Vue from "vue";
import { mount, shallowMount } from "@vue/test-utils";
import MapView from "../src/sealevel2100/views/Map.vue";
import LControl from "../src/sealevel2100/components/Leaflet/LControl.vue";
import LMap from "../src/sealevel2100/components/Leaflet/LMap.vue";
import ScenarioInfo from "../src/sealevel2100/components/Scenario/ScenarioInfo.vue";
import ScenarioSlider from "../src/sealevel2100/components/Scenario/ScenarioSlider.vue";
import Loader from "../src/sealevel2100/components/Base/Loader.vue";
import InfoOverlay from "../src/sealevel2100/components/Base/InfoOverlay.vue";

// Needs a major refactor to split the first test into separate tests that test for
// - initial output
// - output when scenarios are loaded

jest.mock("../src/sealevel2100/EventBus");
jest.mock("../src/sealevel2100/util");

describe("MapView", () => {
    beforeAll(() => {
        Vue.nonreactive = jest.fn();
    });
    afterEach(() => {
        jest.restoreAllMocks();
    });
    it("contains a visible Loader, LMap, LControl and the scenario control elements by default", async () => {
        const vm = mount(MapView);
        await Vue.nextTick();
        expect(vm.findComponent(Loader).exists()).toBeTruthy();
        expect(vm.findComponent(Loader).props("loading")).toBe(true);
        expect(vm.findComponent(LMap).exists()).toBeTruthy();
        expect(vm.findComponent(LControl).exists()).toBeTruthy();
        expect(vm.findComponent(ScenarioSlider).exists()).toBeTruthy();
        expect(vm.findComponent(ScenarioInfo).exists()).toBeTruthy();
        expect(vm.findComponent(InfoOverlay).exists()).toBeTruthy();
    });
    it("hides the loader when loading has finished", async () => {
        const vm = shallowMount(MapView);
        await vm.setData({ loaded: true });
        expect(vm.findComponent(Loader).props("loading")).toBe(false);
    });
});
