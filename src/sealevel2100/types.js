// Scenario Types

/**
 * Information about a single scenario
 * @typedef {Object} ScenarioInfo
 * @property {number} people_affected The amount of people affected by that scenario
 * @property {number} flooded_area The area that gets flooded in this scenario
 * @property {null|Array} part_affected Areas that are particularly affected
 */

/**
 * A single scenario
 * @typedef {Object} Scenario
 * @property {string} url The url to the dataset
 * @property {null | L.GeoJSON | L.GeoJSON[] | L.TileLayer} features A L.GeoJSON object,
 * a list of L.GeoJSON objects or a L.TileLayer object. null if unloaded
 * @property {ScenarioInfo} info Information about a scenario
 */

/**
 * A Scenario consisting of combined information for dikes and no dikes
 * @typedef {Object} CombinedScenario
 * @property {null|Scenario} dikes The scenario with dikes
 * @property {null|Scenario} no_dikes The scenario without dikes
 */

/**
 * Combined Scenarios RCP45, RCP85 and RISE14
 * @typedef {Object} CombinedScenarios
 * @property {CombinedScenario} rcp45 RCP45 scenario
 * @property {CombinedScenario} rcp85 RCP85 scenario
 * @property {CombinedScenario} rise14 RISE14 scenario
 */

/**
 * A single merged Scenario
 * @typedef {Object} MergedScenario
 * @property {string} title The title of the scenario
 * @property {Scenario} dikes The merged scenarios with dikes for North and Baltic Sea
 * @property {Scenario} no_dikes  The merged scenarios without dikes for North and Baltic Sea
 * @property {null|Object|string} meta Meta information about the scenario in general
 */

/**
 * Merged Scenarios RCP45, RCP85 and RISE14
 * @typedef {Object} MergedScenarios
 * @property {MergedScenario} rcp45 RCP45 scenario
 * @property {MergedScenario} rcp85 RCP85 scenario
 * @property {MergedScenario} rise14 RISE14 scenario
 */
