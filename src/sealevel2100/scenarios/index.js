export { default as northsea } from "./northsea";
export { default as balticsea } from "./balticsea";

const rcp45 = `Die Simulation des Meeresspiegelanstiegs basiert auf dem \
gemäßigten IPCC-Szenario RCP4.5, bei dem die Temperatur im Mittel um ca. 1,8 °C \
ansteigt.`;

const rcp85 = `Die Simulation des Meeresspiegelanstiegs basiert auf dem \
IPCC-Szenario RCP8.5, bei dem keine zusätzlichen Anstrengungen zur Begrenzung der \
Emissionen unternommen werden und bei dem die Temperatur im Mittel um ca. 3,7 °C \
ansteigt.`;

const rise14 = `Seit dem IPCC-Bericht im Jahr 2013 gibt es neue Erkenntnisse. \
Komponenten, die bisher unterschätzt wurden, sind das Inlandeis Grönlands und der \
Antarktis sowie Permafrost, dessen Temperatur zu Rekordhöhen angestiegen ist, und \
durch das Freisetzen von CO2 den globalen Temperaturanstieg beschleunigen könnte. \
Aufgrund neuer Beobachtungen, Modellierungen und Expertenbewertungen gehen Forschende \
von einem Ansteig aus, der den vom IPCC prognostizierten Anstieg um mehr als das \
Doppelte übersteigt.`;

export const descriptions = {
    rcp45,
    rcp85,
    rise14,
};

export const ipccDescription = `Um Voraussagen zum Klima zu treffen, stellt der \
Weltklimarat IPCC sog. Representative Concentration Pathways (RCP) auf. Diese \
beschreiben verschiedene Szenarien, wie sich bis 2100 Treibhausgasemissionen, \
atmosphärische Konzentration, Luftschadstoffemissionen und Landnutzung entwickeln \
können.
`;

export const simulationFactors = [
    "Digitales Höhenmodell (TanDEM-X)",
    "Vertikale Landbewegung (IDW-Interpolation aus GPS-Messungen)",
    "Prognosen zum Meeresspiegelanstieg für 2100 (IPCC 2014 und neueste Forschungserkenntnisse)",
    "Mittlerer Hochwasserstand (IDW-Interpolation aus 165 Messstationen)",
];

export const publications = [
    {
        title: `Schuldt et al., 2020 - Sea-Level Rise in Northern Germany: A GIS-Based \
            Simulation and Visualization`,
        link: "https://link.springer.com/article/10.1007/s42489-020-00059-8",
    },
];
