/// <reference path="../../types.js" />

import rcp45 from "./rcp45.json";
import rcp85 from "./rcp85.json";
import rise14 from "./rise14.json";

/** @type {CombinedScenario} */
export default {
    rcp45,
    rcp85,
    rise14,
};
