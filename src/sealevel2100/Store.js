class Store {
    constructor() {
        this._sliderpos = 0;
    }

    get mapObject() {
        return this._mapObject;
    }

    set mapObject(value) {
        this._mapObject = value;
    }

    get sliderPosition() {
        return this._sliderpos;
    }

    set sliderPosition(value) {
        this._sliderpos = value;
    }

    get infoOverlay() {
        return this._infoOverlay;
    }

    set infoOverlay(value) {
        this._infoOverlay = value;
    }
}

export default new Store();
